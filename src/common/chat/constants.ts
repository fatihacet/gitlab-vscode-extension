export const SPECIAL_MESSAGES = {
  RESET: '/reset',
  CLEAR: '/clear',
  CLEAN: '/clean', // Deprecated, but remains for older versions of GitLab
};

export const PLATFORM_ORIGIN = 'vs_code_extension';
