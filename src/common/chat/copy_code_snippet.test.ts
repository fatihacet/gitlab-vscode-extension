import * as vscode from 'vscode';
import { copyCodeSnippet } from './copy_code_snippet';

describe('copyCodeSnippet', () => {
  it('should copy snippet to clipboard and show success message', async () => {
    const snippet = 'const test = "hello";';
    const writeTextSpy = jest.spyOn(vscode.env.clipboard, 'writeText').mockResolvedValue();
    const showInfoMessageSpy = jest
      .spyOn(vscode.window, 'showInformationMessage')
      .mockResolvedValue(undefined);

    await copyCodeSnippet(snippet);

    expect(writeTextSpy).toHaveBeenCalledWith(snippet);
    expect(showInfoMessageSpy).toHaveBeenCalledWith('Snippet copied to clipboard.');
  });

  it('should show error message when copying fails', async () => {
    const snippet = 'const test = "hello";';
    const error = new Error('Failed to copy');
    jest.spyOn(vscode.env.clipboard, 'writeText').mockRejectedValue(error);
    const showErrorMessageSpy = jest
      .spyOn(vscode.window, 'showErrorMessage')
      .mockResolvedValue(undefined);

    await copyCodeSnippet(snippet);

    expect(showErrorMessageSpy).toHaveBeenCalledWith('Error copying snippet: Failed to copy');
  });
});
